#!/usr/bin/python3
# -*- coding: utf-8 -*-

from xml.sax import make_parser
from xml.sax.handler import ContentHandler
import socket
import sys
import hashlib
import simplertp
import secrets
import random
from datetime import datetime


class SmallSMILHandler(ContentHandler):

    def __init__(self):
        self.listha = {}
        self.actualeti = ''
        self.valueses = {'account': ['usern', 'constr'],
                         'uaserver': ['ip', 'oporto'],
                         'rtpaudio': ['oporto'],
                         'regproxy': ['ip', 'oporto'],
                         'log': [''],
                         'audio': ['']}

    def startElement(self, name, atrs):
        self.actualeti = name
        if name in self.valueses:
            for att in self.valueses[name]:
                self.listha[name + ' ' + att] = atrs.get(att, '')

    def endElement(self, name):
        self.actualeti = ''

    def characters(self, content):
        if self.actualeti in ['log', 'audio']:
            self.listha[self.actualeti] = content

    def get_tags(self):
        return self.listha


def log(menssage, logs):
    filee = open(logs, "a")
    formatho = "%Y-%m-%d %H:%M:%S "
    dathe = datetime.now().strftime(formatho)
    filee.write(dathe)
    filee.write(menssage.replace("\r\n", " ") + "\n")
    filee.close()


def response(nonce, consrtd):
    digest = hashlib.sha256()
    digest.update(bytes(nonce + consrtd, 'utf-8'))
    digest.digest()
    return digest.hexdigest()


if __name__ == "__main__":

    arxivo = sys.argv[1]
    parser = make_parser()
    Jandler = SmallSMILHandler()
    parser.setContentHandler(Jandler)
    try:
        parser.parse(open(arxivo))
    except FileNotFoundError:
        sys.exit("File Not Found")
    DIC = Jandler.get_tags()

    try:
        methodo = sys.argv[2].upper()
        menssage = sys.argv[3]
    except ValueError:
        sys.exit("Usage: python3 uaclient.py config method option")
    except IndexError:
        sys.exit("Usage: python3 uaclient.py config method option")
    if DIC['uaserver ip'] == " ":
        IPDIR = "127.0.0.1"
    else:
        IPDIR = DIC['uaserver ip']

    OPORTO = DIC['uaserver oporto']
    USUARIO = DIC['account usern']
    consrtd = DIC['account constr']
    OPORTORTP = DIC['rtpaudio oporto']
    PROXYIPDIR = DIC['regproxy ip']
    PROXYOPORTO = DIC['regproxy oporto']
    LOG = DIC['log']
    SOUNIDO = DIC['audio']
    LINHEA = ("v=0\r\n" + "o=" + USUARIO + " " + IPDIR + "\r\n"
              + "s=iepaEmiliosisoy\r\n" "t=0\r\n"
              + "m=audio " + OPORTORTP + " RTP")
    longithud = len(LINHEA)

    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
        my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        my_socket.connect((PROXYIPDIR, int(PROXYOPORTO)))
        log("Starting...", LOG)

        if methodo == 'INVITE':
            LINHE = (methodo + " sip:" + menssage + " SIPDIR/2.0" "\r\n"
                     + "Content-Type: application/sdp\r\n" + "Content-Length: "
                     + str(longithud) + "\r\n\r\n" + LINHEA)
            log("Sent to " + IPDIR + ":" + OPORTO + ": " + LINHE, LOG)
        elif methodo == 'REGISTER':
            LINHE = (methodo + " sip:" + USUARIO + ":" + OPORTO
                     + " SIPDIR/2.0" "\r\n"
                     + "Expires:" + str(menssage) + "\r\n")
            log("Sent to " + IPDIR + ":" + OPORTO + ": " + LINHE, LOG)
        elif methodo == 'BYE':
            LINHE = (methodo + " sip:" + menssage + " SIPDIR/2.0")
            log("Sent to " + IPDIR + ":"
                + OPORTO + ": " + LINHE, LOG)
        elif methodo != 'REGISTER' and methodo != 'INVITE'\
                and methodo != 'BYE':
            LINHE = (methodo + " sip:" + menssage + " SIPDIR/2.0")
            log("Sent to " + IPDIR + ":" + OPORTO + ": " + LINHE, LOG)

        my_socket.send(bytes(LINHE, 'utf-8') + b'\r\n')
        data = my_socket.recv(1024)

        menssageser = data.decode('utf-8')
        log("Recived from: " + IPDIR + ":" + OPORTO + ": " + menssageser, LOG)
        if 'SIPDIR/2.0 400 Bad Request' in menssageser:
            sys.exit("Recibido: Peticion mal realizada")

        if methodo == 'INVITE':
            respuesthaser = data.decode('utf-8')
            if 'SIPDIR/2.0 404 User Not Found' in respuesthaser:
                sys.exit("Recibido: Usuario no encontrado, necesita registro")
            respuesthaser2 = respuesthaser.split("\r\n")[4]
            if respuesthaser2 == 'SIPDIR/2.0 200 OK':
                trpip = menssageser.split("\r\n")[9].split(" ")[1]
                oportortp = int(menssageser.split("\r\n")[12].split(" ")[1])
                LINHE2 = ('ACK' + ' sip:' + menssage + " SIPDIR/2.0")
                log("Sent to " + IPDIR + ":" + OPORTO + ": " + LINHE2, LOG)
                my_socket.send(bytes(LINHE2, 'utf-8') + b'\r\n')
                BIT = secrets.randbelow(1)
                RTP_header = simplertp.RtpHeader()
                RTP_header.set_header(version=2, marker=BIT,
                                      payload_type=14, ssrc=999999)
                csrc = [random.randint(1, 5), random.randint(1, 6),
                        random.randint(1, 7)]
                RTP_header.setCSRC(csrc)
                audio = simplertp.RtpPayloadMp3(SOUNIDO)
                simplertp.send_rtp_packet(RTP_header, audio, trpip, oportortp)

        if methodo == 'REGISTER':
            respuesthaservidor = menssageser.split("\r\n")[0]
            if respuesthaservidor == 'SIPDIR/2.0 401 Unauthorized':
                nonce = menssageser.split("\r\n")[1].split("=")[1]\
                    .split("\"")[1]
                linea2 = (methodo + " sip:" + USUARIO + ":" + OPORTO +
                          " SIPDIR/2.0" "\r\n" + "Expires:"
                          + str(menssage) + "\r\n")
                lineanueva = linea2 + 'Authorization: Digest response="' \
                                    + response(nonce, consrtd) + '"'
                my_socket.send(bytes(lineanueva, 'utf-8') + b'\r\n\r\n')
                log("Sent to " + IPDIR + ":" + OPORTO + ": " + lineanueva, LOG)
                data = my_socket.recv(1024)
                log("Recived from: " + IPDIR + ":" + OPORTO + ": "
                    + str(data), LOG)

        log("Finishing.", LOG)
