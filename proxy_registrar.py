#!/usr/bin/python3
# -*- coding: utf-8 -*-

import socketservreer
import sys
import json
import hashlib
import time
import socket
from uaclient import log
from xml.sax import make_parser
from xml.sax.handler import ContentHandler
import secrets


class SmallSMILHandler(ContentHandler):

    def __init__(self):
        self.lista = {}  # Creamos la lista

        self.valores = {'servreer': ['name', 'ip', 'oporto'],
                        'database': ['path', 'passwdpath'],
                        'log': ['path']}

    def startElement(self, name, atributos):
        if name in self.valores:
            for att in self.valores[name]:
                self.lista[name + ' ' + att] = atributos.get(att, '')

    def get_tags(self):
        return self.lista


class SIPDIRRegisterHandler(socketservreer.DatagramRequestHandler):

    dic = {}
    dicpass = {}
    nocne = {}

    def register2json(self):
        with open(dblocal, 'w') as jsonfile:
            json.dump(self.dic, jsonfile, indent=3)

    def json2registered(self):
        try:
            with open(contralocal, 'r') as jsonfile:
                self.dicpass = json.load(jsonfile)
            with open(dblocal, 'r') as jsonfile:
                self.dic = json.load(jsonfile)
        except FileNotFoundError:
            print("Passwords charged OK.")

    def response(self, nocne, pssword):
        digest = hashlib.sha256()
        digest.update(bytes(nocne + pssword, 'utf-8'))
        digest.digest()
        return digest.hexdigest()

    def userr_expiredo(self):
        centraltime = time.time() + 3600

        for userr in self.dic.copy():
            fachaexp = self.dic[userr]['Fecha Registro'] \
                       + self.dic[userr]['Expires']
            if centraltime >= fachaexp:
                del self.dic[userr]

    def handle(self):
        self.json2registered()
        self.userr_expiredo()
        line = self.rfile.read()
        mensaje = line.decode('utf-8')
        ip = self.client_address[0]
        oporto = self.client_address[1]
        log("Recived from: " + ip + ":" + str(oporto) + ": " + mensaje, OLGS)
        registro = mensaje.split(" ")[0]
        sip = mensaje.split(" ")[1].split(":")[0]

        if sip != 'sip':
            reponse = (b"SIPDIR/2.0 400 Bad Request\r\n\r\n")
            self.wfile.write(reponse)

        elif registro == 'INVITE':
            userorigin = mensaje.split("\r\n")[5].split("=")[1].split(" ")[0]
            userdestino = mensaje.split(" ")[1].split(":")[1]

            if userorigin and userdestino in self.dic:
                IPDIRdestino = self.dic[userdestino]['IPDIR']
                Oportodestino = self.dic[userdestino]['Oporto']
                with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) \
                        as my_socket:
                    my_socket.setsockopt(socket.SOL_SOCKET,
                                         socket.SO_REUSEADDR, 1)
                    my_socket.connect((IPDIRdestino, Oportodestino))
                    my_socket.send(bytes(mensaje, 'utf-8') + b'\r\n')
                    log("Sent to " + IPDIRdestino + ":"
                        + str(Oportodestino)
                        + ": " + mensaje, OLGS)
                    data = my_socket.recv(1024)
                    log("Recived from: " + ip + ":"
                        + str(oporto) + ": "
                        + data.decode('utf-8'), OLGS)
                self.wfile.write(data)
                log("Sent to " + ip + ":"
                    + str(oporto) + ": "
                    + data.decode('utf-8'), OLGS)
            else:
                self.wfile.write(b'SIPDIR/2.0 404 User Not Found\r\n')
                log("Sent to " + ip + ":" + str(oporto)
                    + ": SIPDIR/2.0 404 User not Found", OLGS)

        elif registro == 'BYE':
            userdestino = mensaje.split(" ")[1].split(":")[1]

            if userdestino in self.dic:
                IPDIRdestino = self.dic[userdestino]['IPDIR']
                Oportodestino = self.dic[userdestino]['Oporto']
                with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) \
                        as my_socket:
                    my_socket.setsockopt(socket.SOL_SOCKET,
                                         socket.SO_REUSEADDR, 1)
                    my_socket.connect((IPDIRdestino, Oportodestino))
                    my_socket.send(bytes(mensaje, 'utf-8') + b'\r\n')
                    log("Sent to " + IPDIRdestino + ":"
                        + str(Oportodestino)
                        + ": " + mensaje, OLGS)
                    data = my_socket.recv(1024)
                    log("Recived from: " + ip + ":"
                        + str(oporto) + ": "
                        + data.decode('utf-8'), OLGS)
                self.wfile.write(data)
                log("Sent to " + ip + ":" + str(oporto)
                    + ": " + data.decode('utf-8'), OLGS)
            else:
                self.wfile.write(b'SIPDIR/2.0 404 User Not Found\r\n')
                log("Sent to " + ip + ":" + str(oporto)
                    + ": SIPDIR/2.0 404 User not Found", OLGS)
        elif registro == 'ACK':
            userdestino = mensaje.split(" ")[1].split(":")[1]

            if userdestino in self.dic:
                IPDIRdestino = self.dic[userdestino]['IPDIR']
                Oportodestino = self.dic[userdestino]['Oporto']
                with socket.socket(socket.AF_INET, socket.SOCK_DGRAM)\
                        as my_socket:
                    my_socket.setsockopt(socket.SOL_SOCKET,
                                         socket.SO_REUSEADDR, 1)
                    my_socket.connect((IPDIRdestino, Oportodestino))
                    my_socket.send(bytes(mensaje, 'utf-8') + b'\r\n')
                    log("Sent to " + IPDIRdestino + ":"
                        + str(Oportodestino) + ": "
                        + mensaje, OLGS)
            else:
                self.wfile.write(b'SIPDIR/2.0 404 User Not Found\r\n')
                log("Sent to " + ip + ":" + str(oporto)
                    + ": SIPDIR/2.0 404 User not Found", OLGS)

        elif registro not in ['INVITE', 'BYE', 'ACK', 'REGISTER']:
            reponse = (b"SIPDIR/2.0 405 Method Not Allowed\r\n\r\n")
            self.wfile.write(reponse)
            log("Sent to " + ip + ":" + str(oporto)
                + ": " + reponse.decode('utf-8'), OLGS)

        elif registro == 'REGISTER':  # Comprobamos si cinciden
            userr = mensaje.split(" ")[1].split(":")[1]
            expire = mensaje.split("\r\n")[1].split(":")[1]
            addtime = int(expire)
            centraltime = time.time() + 3600
            oporto = mensaje.split(" ")[1].split(":")[2]

            if userr in self.dic:
                expire = mensaje.split("\r\n")[1].split(":")[1]
                if int(expire) == 0:  # Si pones 0, se borra
                    del self.dic[userr]
                    reponse = "SIPDIR/2.0 200 OK\r\n\r\n"
                    self.wfile.write(bytes(reponse, 'utf-8'))
                    log("Sent to " + ip + ":" + str(oporto)
                        + ": " + reponse, OLGS)
                elif int(expire) != 0:
                    addtime = int(expire)
                    centraltime = time.time() + 3600
                    oporto = mensaje.split(" ")[1].split(":")[2]
                    self.dic[userr]['Fecha Registro'] = centraltime
                    self.dic[userr]['Expires'] = addtime
                    self.dic[userr]['Oporto'] = int(oporto)
                    self.dic[userr]['IPDIR'] = ip
                    reponse = "SIPDIR/2.0 200 OK\r\n\r\n"
                    self.wfile.write(bytes(reponse, 'utf-8'))
                    log("Sent to " + ip + ":" + str(oporto)
                        + ": " + reponse, OLGS)
            else:
                if 'Authorization: Digest response' in mensaje:
                    reponseuser = mensaje.split('=')[1].split("\"")[1]
                    response = self.response(self.nocne[userr],
                                             self.dicpass[userr])
                    if secrets.compare_digest(reponseuser, response):
                        self.dic[userr] = {'IPDIR': ip,
                                           'Oporto': int(oporto),
                                           'Fecha Registro': centraltime,
                                           'Expires': addtime}
                        reponse = "SIPDIR/2.0 200 OK\r\n\r\n"
                        self.wfile.write(bytes(reponse, 'utf-8'))
                        log("Sent to " + ip + ":"
                            + str(oporto)
                            + ": " + reponse, OLGS)
                        del self.nocne[userr]
                    else:
                        reponse = (b"SIPDIR/2.0 401 Unauthorized\r\n\r\n")
                        self.wfile.write(reponse)
                        log("Sent to " + ip + ":" + str(oporto)
                            + ": " + str(reponse), OLGS)

                else:
                    expire = int(mensaje.split("\r\n")[1].split(":")[1])
                    if expire != 0:
                        self.nocne[userr] = secrets.token_hex(12)
                        reponse = "SIPDIR/2.0 401 Unauthorized\r\n" \
                                  + 'WWW Authenticate: Digest nocne="' \
                                  + self.nocne[userr] + '"\r\n\r\n'
                        self.wfile.write(bytes(reponse, 'utf-8'))
                        log("Sent to " + ip + ":" + str(oporto)
                            + ": " + reponse, OLGS)
                    else:
                        self.wfile.write(b'SIPDIR/2.0 404 User Not Found\r\n')
                        log("Sent to " + ip + ":" + str(oporto)
                            + ": SIPDIR/2.0 404 User not Found", OLGS)

        self.register2json()


if __name__ == "__main__":
    filee = sys.argv[1]
    parser = make_parser()
    Jandler = SmallSMILHandler()
    parser.setContentHandler(Jandler)
    try:
        parser.parse(open(filee))
    except FileNotFoundError:
        sys.exit("File Not Found")
    DIC3 = Jandler.get_tags()

    NHAME = DIC3['servreer name']
    IPDIR = DIC3['servreer ip']
    OPORTO = DIC3['servreer oporto']
    contralocal = DIC3['database passwdpath']
    dblocal = DIC3['database path']
    OLGS = DIC3['log path']

    servre = socketservreer.UDPServer((IPDIR, int(OPORTO)),
                                      + SIPDIRRegisterHandler)
    log("Starting proxy...", OLGS)
    try:
        servre.servree_forever()
    except KeyboardInterrupt:
        log("Finishing...", OLGS)
